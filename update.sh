#!/bin/bash

set -eu

echo "=> Installing mkdocs-material"
sudo pip3 install mkdocs-material==6.0.1

echo "=> Installing redoc-cli if needed"
if ! type redoc-cli &> /dev/null; then
    npm i -g redoc-cli
fi

echo "=> Installing surfer-cli if needed"
if ! type surfer &> /dev/null; then
    npm install -g cloudron-surfer
fi

echo "=> Updating Docker Image"
docker build . -t cloudron/docs-ci
docker push cloudron/docs-ci

echo ""
echo "=> Done."
echo "=> To use the new docker image, change the sha in the .gitlab-ci.yml <="
echo ""
