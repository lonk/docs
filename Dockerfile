FROM cloudron/base:2.0.0

RUN apt-get update && \
    apt install python3-setuptools && \
    pip3 install mkdocs-material==6.0.1 && \
    npm i -g redoc-cli && \
    npm install -g cloudron-surfer@5.10.4

