# <img src="/img/pixelfed-logo.png" width="25px"> Pixelfed App

## Admin

To make a register user an admin, use the [Web terminal](/apps#web-terminal) and run
the following command:

```
# sudo -u www-data php artisan user:admin username_here

Found username: girish

 Add admin privileges to this user? (yes/no) [no]:
 > yes

Successfully changed permissions!
```

Further administration commands like removing a user, removing unused media can be found in
[Pixelfed docs](https://docs.pixelfed.org/running-pixelfed/administration.html#admin-user).

## Federation

To test if federation works, search for a handle like `girish@pixelfed.social`. You should then
be able to follow that handle. Note that you can only see posts that are made _after_ you followed
the handle. Existing posts will **not** appear in you stream.

If following doesn't work:

* Login as an admin user on your Pixelfed instance
* Check `https://pixelfed.domain.com/horizon/failed` for job failures 

## Customizations

You can find the configuration file (env.production) under `/app/data`. Don`t forget to restart the app
after making any changes to this file.

