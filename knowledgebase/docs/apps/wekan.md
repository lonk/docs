# <img src="/img/wekan-logo.png" width="25px"> Wekan App

## API

Wekan has a [REST API](https://github.com/wekan/wekan/wiki/REST-API) for
managing users.

## Webbooks

When a webhook is activated, Wekan sends the related information within the POST request body.
See [Webhook-data](https://github.com/wekan/wekan/wiki/Webhook-data) for details.

## Users

Wekan currently does not support syncing or searching users via LDAP. This limitation means
that a user must login to Wekan first before they become available for sharing
boards.

## Admin

To make a Cloudron user an admin, first make sure the user has logged once into Wekan (this
creates the user in Wekan). Then, run the following command after replacing `ADMIN_USERNAME` below
in the [Web Terminal](/apps#web-terminal):

```
# mongo -u "${CLOUDRON_MONGODB_USERNAME}" -p "${CLOUDRON_MONGODB_PASSWORD}" ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE} --eval "db.users.update({ username: 'ADMIN_USERNAME' }, { \$set: {isAdmin: true } })"

MongoDB shell version v3.6.3
connecting to: mongodb://mongodb:27017/b52e1fdb-5b2d-417e-a41c-53d060a97141
MongoDB server version: 3.6.3
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
```

## Registration

External user registration is disabled by default. Note that the register link is still visible
in the login screen even when registration is disabled ([Wekan issue](https://github.com/wekan/wekan/issues/2839)).
User registration can be enabled from the Wekan's Admin Panel. The Wekan Admin Panel option is only
visible when the Cloudron user is a Wekan administrator.

## Customization

Custom env vars can be added in `/app/data/env`. Be sure to restart the app after editing this file.

