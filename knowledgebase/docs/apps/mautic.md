# <img src="/img/mautic-logo.png" width="25px"> Mautic App

## Cron

For cron support, edit the file `/app/data/crontab.user`.

The file can be edited using the [Web terminal](/apps#web-terminal)
or the [File manager](/apps/#file-manager).

The crontab contains a line like:

```
0,15,30,45 * * * *  sudo -E -u www-data php /app/code/app/console mautic:integration:fetchleads --integration=Hubspot > /proc/$(cat /var/run/crond.pid)/fd/1 2>&1
```

The app must be restarted after making any changes to the `crontab`
file. You can do this by pressing the 'Restart' button in the web terminal.

