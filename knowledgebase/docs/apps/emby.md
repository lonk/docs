# <img src="/img/emby-logo.png" width="25px"> Emby App

## Mobile Apps

Emby Apps for various devices can be downloaded [here](https://emby.media/download.html).

For iOS and Android, you should be able to connect simply using the `https://emby.mydomain.com`.
in the custom server url field. No further ports or custom api urls need to be specified.

!!! note "Use port 443"
    When connecting with the mobile apps use port 443 and not port 8920.

## Hardware Transcoding

!!! note "Cloudron 5.6 required"
    Cloudron 5.6 is the first release that supports hardware transcoding.

Emby supports 3 types of [hardware acceleration](https://support.emby.media/support/solutions/articles/44001160207)
on Linux - Nvidia NVDEC, VA API and Intel QuickSync. Cloudron does not support Nvidia at the time of this
writing.

There are various steps to check if your hardware supports transcoding and if Emby is able to take
advantage of it.

* Check the output of `vainfo` on your server. You might have to run `apt-get install vainfo libva2 i965-va-driver`
  if that command is not available on your server. The output should look like below. `VAEntrypointVLD` means that
    your card is capable to decode this format, `VAEntrypointEncSlice` means that you can encode to this format.

```
$ vainfo 
error: can't connect to X server!
libva info: VA-API version 1.1.0
libva info: va_getDriverName() returns 0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/i965_drv_video.so
libva info: Found init function __vaDriverInit_1_1
libva info: va_openDriver() returns 0
vainfo: VA-API version: 1.1 (libva 2.1.0)
vainfo: Driver version: Intel i965 driver for Intel(R) CherryView - 2.1.0
vainfo: Supported profile and entrypoints
      VAProfileMPEG2Simple            :	VAEntrypointVLD
      VAProfileMPEG2Simple            :	VAEntrypointEncSlice
      VAProfileMPEG2Main              :	VAEntrypointVLD
      VAProfileMPEG2Main              :	VAEntrypointEncSlice
      VAProfileH264ConstrainedBaseline:	VAEntrypointVLD
      VAProfileH264ConstrainedBaseline:	VAEntrypointEncSlice
      VAProfileH264Main               :	VAEntrypointVLD
      VAProfileH264Main               :	VAEntrypointEncSlice
      VAProfileH264High               :	VAEntrypointVLD
      VAProfileH264High               :	VAEntrypointEncSlice
      VAProfileH264MultiviewHigh      :	VAEntrypointVLD
      VAProfileH264MultiviewHigh      :	VAEntrypointEncSlice
      VAProfileH264StereoHigh         :	VAEntrypointVLD
      VAProfileH264StereoHigh         :	VAEntrypointEncSlice
      VAProfileVC1Simple              :	VAEntrypointVLD
      VAProfileVC1Main                :	VAEntrypointVLD
      VAProfileVC1Advanced            :	VAEntrypointVLD
      VAProfileNone                   :	VAEntrypointVideoProc
      VAProfileJPEGBaseline           :	VAEntrypointVLD
      VAProfileJPEGBaseline           :	VAEntrypointEncPicture
      VAProfileVP8Version0_3          :	VAEntrypointVLD
      VAProfileVP8Version0_3          :	VAEntrypointEncSlice
      VAProfileHEVCMain               :	VAEntrypointVLD
```

* Next step is to check Emby logs. In `Manage Emby Server` -> `Advanced` -> `Logs`, look for a file named
  `hardware_detection-<something>.txt`. If you cannot find this file, simply restart Emby and it will appear on 
  startup. The log file output will indicate that it detected the DRI device and can access it and what it can transcode.

* Now that Emby can access the DRI devices, play a video. For every video played, Emby will generate a log file of
  the `ffmpeg-transcode-<something>.txt`. The log file outputs the Porcessing plan. Finally, when the video is playing,
  open a new browser tab and see the `Active Devices` in the Emby dashboard. This will show that the video is indeed
  transcoding.

<center>
<img src="/img/emby-active-devices.png" class="shadow">
</center>

