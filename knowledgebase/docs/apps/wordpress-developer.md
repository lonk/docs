# <img src="/img/wordpress-developer-logo.png" width="25px"> WordPress (Developer) App

!!! note "WordPress (Developer)"
    This app is targeted at users who want to have complete control over their WordPress installation.
    The WordPress code can be accessed and edited via SFTP. WordPress' built-in updater has to be used
    to periodically check and install updates. If you prefer delegating the responsibility of applying
    updates to the Cloudron team, use the [WordPress Managed app](/apps/wordpress-managed) instead.

## Admin page

The WordPress admin page is located `https://<my.example.com>/wp-login.php`.

## Using SFTP

The app can be uploaded using an SFTP client like [FileZilla](https://filezilla-project.org/).

You can find the SFTP login details when clicking on the `i` icon in the app grid.

<center>
<img src="/img/lamp-filezilla.png" class="shadow">
</center>

!!! note "SFTP Access"
    SFTP access for non-admin users can be granted using the [access control UI](/apps/#restricting-app-access-to-specific-users).

## Memory limits

To adjust memory allocated for WordPress, edit `/app/data/wp-config.php` using the [Web Terminal](/apps#web-terminal)
and add the following line at the end of the file:

```
define('WP_MEMORY_LIMIT', '128M');
define('WP_MAX_MEMORY_LIMIT', '256M');
```

Note that the app also has a separate memory limit controlled by the app's [memory limit](https://cloudron.io/apps/#increasing-the-memory-limit-of-an-app). If you increase `WP_MEMORY_LIMIT`, be sure to increase the app's memory limit. A good formula is to provide the app 6 times the `WP_MEMORY_LIMIT` value at the bare minimum.

`WP_MAX_MEMORY_LIMIT` is the limit for administration tasks, which often require more.

A detailed explanation can be found in the [WordPress docs](https://wordpress.org/support/article/editing-wp-config-php/#increasing-memory-allocated-to-php).

## htaccess

By default the app does not have an `.htaccess` file. It can be added via [SFTP](/apps/wordpress-Developer/#using-sftp) or the [Web terminal](/apps#web-terminal) into the app at `/app/data/public/.htaccess` for `/` or depending on where it is required in any of the other WordPress related subfolders in `/app/data/public/`.

## Cron tasks

The app is configured to run WordPress cron tasks every minute.

To run the cron tasks manually run the following command using the
[Web terminal](/apps#web-terminal):

```
wp cron event run --due-now
```

WordPress' built-in cron task schedule `wp-cron` is disabled since
it is [not effective](https://www.lucasrolff.com/wordpress/why-wp-cron-sucks/)
for low traffic websites.

To add custom cron events, use a plugin like [WP Crontrol](https://wordpress.org/plugins/wp-crontrol/).

## Plugins

Unlike the [Managed WordPress app](/apps/wordpress-managed), you can install
plugins that modify the code.

## Performance

[GTmetrix](https://gtmetrix.com) is a great site for getting performance metrics on the
WordPress installation.

* To set the expires headers for all pages, the [WP Fastest Cache](https://wordpress.org/plugins/wp-fastest-cache/)
  plugin can be installed.

* For CDN caching, we recommend [WP Fastest Cache](https://wordpress.org/plugins/wp-fastest-cache/) or
[W3 Total Cache](https://wordpress.org/plugins/w3-total-cache/) for CDN based cache. Ryan Kite has a
[good tutorial](https://ryan-kite.com/how-to-create-a-cdn-for-wp-fastest-cache-with-aws-cloudfront/) on
how to setup AWS Cloudfront with WP Fastest Cache.

## Database access

Cloudron does not support PHPMyAdmin. It is, however, possible to access the database
using other methods:

* Open a [Web terminal](/apps#web-terminal) and press the 'MySQL' button to get console
  access. You can execute SQL commands directly.

* Use a plugin like (WP phpMyAdmin)[https://wordpress.org/plugins/wp-phpmyadmin-extension/] for a GUI.

## WP CLI

[WP CLI](http://wp-cli.org/) is a command line interface to WordPress. To run commands
using the CLI tool, open a [Web terminal](/apps#web-terminal) and
execute commands WP CLI using simply `wp`. It is pre-setup to run as the correct user already.

Additional php settings can be configured, when running the cli manually with `php -d key=value`:
```
sudo -E -u www-data php -d max_execution_time=100 /app/pkg/wp
```
In this case setting the maximum execution timeout to 100 seconds.

## PHP settings

You can add custom [PHP settings](http://php.net/manual/en/ini.core.php) in `/app/data/php.ini`

### File upload size

Change the following values in `/app/data/php.ini`:

```
post_max_size = 256M
upload_max_filesize = 256M
memory_limit = 256M
```

## Migrating existing site

See our [blog](https://blog.cloudron.io/migrating-a-wordpress-site-to-cloudron/) on how to migrate an existing
WordPress site to Cloudron.

