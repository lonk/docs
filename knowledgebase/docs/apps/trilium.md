# <img src="/img/trilium-logo.png" width="25px"> Trilium App

## Multiple users

Trilium does not support multiple users. It's a single user application intended
for personal notes. See the [FAQ](https://github.com/zadam/trilium/wiki/FAQ#multi-user-support)
for more information.

