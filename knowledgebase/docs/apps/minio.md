# <img src="/img/minio-logo.png" width="25px"> MinIO App

## Admin credentials

To change admin credentials, use the [Web terminal](/apps#web-terminal) to edit
the `credentials` section in `/app/data/data/.minio.sys/config/config.json`. Note that MinIO does
not save this JSON file with identation/formatting, making it hard to edit it. For this reason,
we have written a small script that is part of the package to edit it easily.

To set credentials:

```
# /app/code/minio-credentials set NEWACCESSKEY NEWSECRETKEY
Credentials updated. Restart minio app for new credentials to take effect.
```

To get the current credentials (in case you forgot it):

```
# /app/code/minio-credentials get
Access Key: NEWACCESSKEY
Secret Key: NEWSECRETKEY
```

!!! note "Access Key Contraints"
    Please see the [AWS Docs](https://docs.aws.amazon.com/IAM/latest/APIReference/API_CreateAccessKey.html) for
    length and pattern restrictions of the access key and secret key. In short, make sure it matches the `[\w+=,.@-]+` regexp
    and has atleast 8 characters

## Cloudron Backup

Cloudron supports [backing up to minio](https://cloudron.io/backups/#backing-up-to-minio).
Backing up a Cloudron to a minio installed in another Cloudron will work fine. However, backing up a
Cloudron to a minio installed in the very same Cloudron is not supported.

## Custom configuration

Custom config variables can be exported in `/app/data/env.sh`. This file is sourced automatically on startup.

