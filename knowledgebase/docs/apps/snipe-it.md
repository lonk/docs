# <img src="/img/snipe-it-logo.png" width="25px"> Snipe-IT App

## Customization

Use the [Web terminal](/apps#web-terminal) to edit custom configuration
under `/app/data/env`.

