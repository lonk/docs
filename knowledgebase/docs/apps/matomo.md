# <img src="/img/piwik-logo.png" width="25px"> Matomo App

## Installing plugins

Matomo plugins can be installed from the Marketplace view. Only the Matomo super-user
can install plugins (Matomo admins cannot install plugins).

Please be aware that there is a bug in Matomo that you [cannot install plugins when LDAP authentication is enabled](https://github.com/matomo-org/matomo/issues/14770).

To workaround this issue:

* Deactivate the LDAP plugin. `Plugins` -> `LoginLdap` -> `Deactivate`.

* Install required plugins

* Re-enable the LDAP plugin

## UI Errors

The matomo UI shows many [intermittent error messages](https://github.com/matomo-org/matomo/issues/10578)
on Firefox. We recommend using an alternate browser.

