# <img src="/img/simple-torrent-logo.png" width="25px"> Simple Torrent App

## Customization

Use the [Web terminal](/apps#web-terminal)
to edit [custom configuration](https://github.com/boypt/simple-torrent/wiki/Config-File)
under `/app/data/config/cloud-torrent.json`.

